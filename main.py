import pygame
import random
import os
# import pyganim

# set up asset folders
game_folder = os.path.dirname(__file__)
img_folder = os.path.join(game_folder, 'img')

WIDTH = 1280
HEIGHT = 720
FPS = 60

# define colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
ORANGE = (244, 208, 63)

# initialize pygame and create window
pygame.init()
pygame.mixer.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Air Boom")
clock = pygame.time.Clock()

font_name = pygame.font.match_font('consolas')
def draw_text(surf, text, size, x, y):
    font = pygame.font.Font(font_name, size)
    text_surface = font.render(text, True, ORANGE)
    text_rect = text_surface.get_rect()
    text_rect.midtop = (x, y)
    surf.blit(text_surface, text_rect)

player_center = pygame.image.load(os.path.join(img_folder, 'fighter.png'))
player_left = pygame.image.load(os.path.join(img_folder, 'fighterL.png'))
player_right = pygame.image.load(os.path.join(img_folder, 'fighterR.png'))
enemy = pygame.image.load(os.path.join(img_folder, 'enemy.png'))
bullet = pygame.image.load(os.path.join(img_folder, 'bullet.png'))
terrain = pygame.image.load(os.path.join(img_folder, 'terrainhd.png')).convert()


# boom_anim = pyganim.PygAnimation([('img/explode00.png', 100), ('img/explode01.png', 100), ('img/explode02.png', 100),
#                                  ('img/explode03.png', 100), ('img/explode04.png', 100), ('img/explode05.png', 100),
#                                  ('img/explode06.png', 100), ('img/explode07.png', 100), ('img/explode08.png', 100),
#                                  ('img/explode09.png', 100), ('img/explode10.png', 100), ('img/explode11.png', 100),
#                                  ('img/explode12.png', 100), ('img/explode13.png', 100), ('img/explode14.png', 100),
#                                  ('img/explode15.png', 100), ('img/explode16.png', 100), ('img/explode17.png', 100),
#                                  ('img/explode18.png', 100), ('img/explode19.png', 100), ('img/explode20.png', 100),
#                                  ('img/explode21.png', 100), ('img/explode22.png', 100), ('img/explode23.png', 100),
#                                  ('img/explode24.png', 100), ('img/explode25.png', 100), ('img/explode26.png', 100),
#                                  ('img/explode27.png', 100), ('img/explode28.png', 100), ('img/explode29.png', 100),
#                                  ('img/explode30.png', 100)])
# boom_anim.play()

tree_images = []
tree_list = ['tree1.png', 'tree2.png', 'tree3.png']

for img in tree_list:
    tree_images.append(pygame.image.load(os.path.join(img_folder, img)))

hp_images = []
hp_list = ['hp1.png', 'hp2.png', 'hp3.png', 'hp4.png', 'hp5.png']

for img2 in hp_list:
    hp_images.append(pygame.image.load(os.path.join(img_folder, img2)))

hp = 4


class Player(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = player_center
        self.rect = self.image.get_rect()
        self.radius = 30
        self.rect.centerx = WIDTH / 2
        self.rect.bottom = HEIGHT - 30
        self.speedx = 0

    def update(self):
        self.speedx = 0
        keystate = pygame.key.get_pressed()

        if keystate[pygame.K_LEFT]:
            self.speedx = -5
            self.image = player_left
        elif keystate[pygame.K_RIGHT]:
            self.speedx = 5
            self.image = player_right
        else:
            self.image = player_center

        self.rect.x += self.speedx
        if self.rect.right >= WIDTH:
            self.rect.right = WIDTH
        if self.rect.left <= 0:
            self.rect.left = 0

    def shoot(self):
        bullet = Bullet(self.rect.centerx, self.rect.top)
        all_sprites.add(bullet)
        bullets.add(bullet)


class Hp_bar(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = hp_images[0]
        self.rect = self.image.get_rect()
        self.rect.centerx = WIDTH - 120
        self.rect.bottom = HEIGHT - 20

    def update(self):
        if hp == 4:
            self.image = hp_images[0]
        elif hp == 3:
            self.image = hp_images[1]
        elif hp == 2:
            self.image = hp_images[2]
        elif hp == 1:
            self.image = hp_images[3]
        elif hp == 0:
            self.image = hp_images[4]


class Terrain(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = terrain
        self.rect = self.image.get_rect()
        self.speedy = 4

    def update(self):
        self.rect.y += self.speedy
        if self.rect.top > 0:
            self.rect.bottom = HEIGHT


class Tree(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image_orig = random.choice(tree_images)
        self.image_orig.set_colorkey(BLACK)
        self.image = self.image_orig.copy()
        self.rect = self.image.get_rect()
        self.rect.x = random.randrange(-50, WIDTH + 50)
        self.rect.y = random.randrange(-50, HEIGHT + 50)
        self.speedy = 4

    def update(self):
        self.rect.y += self.speedy
        if self.rect.top > HEIGHT:
            self.rect.x = random.randrange(-50, WIDTH + 50)
            self.rect.bottom = 0


class Bullet(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = bullet
        self.rect = self.image.get_rect()
        self.rect.bottom = y
        self.rect.centerx = x
        self.speedy = -10

    def update(self):
        self.rect.y += self.speedy
        # kill if it moves off the top of the screen
        if self.rect.bottom < 0:
            self.kill()


class Enemy(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = enemy
        self.rect = self.image.get_rect()
        self.radius = 30
        self.rect.x = random.randrange(WIDTH - self.rect.width)
        self.rect.y = random.randrange(-100, -40)
        self.speedy = random.randrange(6, 10)

    def update(self):
        self.rect.y += self.speedy
        if self.rect.top > HEIGHT + 10:
            self.rect.x = random.randrange(WIDTH - self.rect.width)
            self.rect.y = random.randrange(-100, -40)
            self.speedy = random.randrange(6, 10)


all_sprites = pygame.sprite.Group()
trees = pygame.sprite.Group()
enemies = pygame.sprite.Group()
bullets = pygame.sprite.Group()
terrain = Terrain()
player = Player()
hp_bar = Hp_bar()

all_sprites.add(terrain)

for t in range(50):
    t = Tree()
    all_sprites.add(t)
    trees.add(t)

all_sprites.add(player)

for i in range(5):
    m = Enemy()
    all_sprites.add(m)
    enemies.add(m)
score = 0

all_sprites.add(hp_bar)

# Game loop
running = True
while running:
    # keep loop running at the right speed
    clock.tick(FPS)
    # Process input (events)
    for event in pygame.event.get():
        # check for closing window
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                player.shoot()
    # Update
    all_sprites.update()

    # check to see if a bullet hit a mob
    hits = pygame.sprite.groupcollide(enemies, bullets, True, True)
    for hit in hits:
        score += 100
        m = Enemy()
        all_sprites.add(m)
        enemies.add(m)

    # check to see if a enemy hit the player
    hits = pygame.sprite.spritecollide(player, enemies, True, pygame.sprite.collide_circle)
    if hits:
        hp -= 1
        m = Enemy()
        all_sprites.add(m)
        enemies.add(m)
        if hp == 0:
            running = False

    # Draw / render
    screen.fill(BLACK)
    all_sprites.draw(screen)
    draw_text(screen, str(score), 50, 100, 10)

    # *after* drawing everything, flip the display
    pygame.display.flip()

pygame.quit()